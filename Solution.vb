Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic

Public Class ProductInventory

    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)

    Public Sub New()
        startTime = Environment.TickCount
        limitTime = startTime + 9800
        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))
    End Sub

    Class Item
        Public Buy, Sell, Expiration As Integer
        Public Sub New(b As Integer, s As Integer, e As Integer)
            Buy = b: Sell = s: Expiration = e
        End Sub
    End Class

    Dim N As Integer
    Dim items As New List(Of Item)()
    Dim day As Integer = 0
    Dim history() As Integer
    Dim stocks() As Integer
    Dim selled() As Integer
    Dim expires(,) As Integer
    
    Dim medians() As List(Of Integer)

    Public Function init(buy() As Integer, sell() As Integer, expiration() As Integer) As Integer
        init = 0

        N = buy.Length

        For i As Integer = 0 To UBound(buy)
            items.Add(New Item(buy(i), sell(i), expiration(i)))
        Next i

        ReDim history(N - 1)

        ReDim stocks(N - 1)
        ReDim selled(N - 1)
        ReDim expires(N - 1, 120)
        
        Redim medians(N - 1)
        For i As Integer = 0 To N - 1
            medians(i) = New List(Of Integer)()
        Next i

    End Function


    Public Function order(recentPurchases() As Integer) As Integer()
        day += 1

        If day = 1 Then
            Array.Copy(recentPurchases, history, history.Length)
            For i As Integer = 0 To N - 1
                medians(i).Add((recentPurchases(i) + 9) \ 10)
            Next i
        Else
            For i As Integer = 0 To N - 1
                history(i) += recentPurchases(i)
                medians(i).Add(recentPurchases(i))
                medians(i).Sort()

                selled(i) += recentPurchases(i)
                selled(i) = Math.Max(0, selled(i) - expires(i, day - 1))
                stocks(i) -= expires(i, day - 1)
            Next i
        End If

        Dim ret(N - 1) As Integer
        
        Const HighStock As Integer = 8
        Const MidStock As Integer = 7
        Const LowStock As Integer = 6
        Const MedDay As Integer = 30
        Const FinDay As Integer = 75
        
        For i As Integer = 0 To N - 1
            
            Dim rems As Integer = Math.Max(0, stocks(i) - selled(i))
            Dim stock As Integer = HighStock
            If items(i).Sell - items(i).Buy < items(i).Buy \ 2 Then stock = MidStock
            If items(i).Sell - items(i).Buy < 0 Then stock = LowStock
            
            If day > MedDay Then
                Dim meds As List(Of Integer) = medians(i)
                Dim medi As Integer = meds.Count \ 2
                If meds.Count Mod 2 = 0 Then
                    stock = (meds(medi) + meds(medi - 1)) \ 2
                Else
                    stock = meds(medi)
                End If
                If day < FinDay Then
                    If items(i).Sell - items(i).Buy > items(i).Buy \ 2 Then
                        stock = Math.Min(stock + 4, HighStock)
                    ElseIf items(i).Sell - items(i).Buy >= 0 Then
                        stock = Math.Min(stock + 3, MidStock)
                    ElseIf items(i).Sell - items(i).Buy < 0 Then
                        stock = Math.Min(stock + 2, LowStock)
                    End If
                Else
                    if items(i).Sell - items(i).Buy > items(i).Buy \ 2 Then
                        stock += 2
                    ElseIf items(i).Sell - items(i).Buy >= 0 Then
                        stock += 1
                    End If
                End If
            End If
            
            If rems < stock Then
            
                Dim buy As Integer = stock - rems
                stocks(i) += buy
                expires(i, day + items(i).Expiration) += buy
                ret(i) = buy
            End If
        Next i

        order = ret
    End Function


End Class