Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main


    Function CallInit(_productInventory As ProductInventory) As Integer
        Dim _buySize As Integer = CInt(Console.ReadLine()) - 1
        Dim buy(_buySize) As Integer
        For _idx As Integer = 0 To _buySize
            buy(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _sellSize As Integer = CInt(Console.ReadLine()) - 1
        Dim sell(_sellSize) As Integer
        For _idx As Integer = 0 To _sellSize
            sell(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim _expirationSize As Integer = CInt(Console.ReadLine()) - 1
        Dim expiration(_expirationSize) As Integer
        For _idx As Integer = 0 To _expirationSize
            expiration(_idx) = CInt(Console.ReadLine())
        Next _idx

        Dim _result As Integer = _productInventory.init(buy, sell, expiration)

        Console.WriteLine(_result)
        Console.Out.Flush()

        Return 0
    End Function

    Function CallOrder(_productInventory As ProductInventory) As Integer
        Dim _recentPurchasesSize As Integer = CInt(Console.ReadLine()) - 1
        Dim recentPurchases(_recentPurchasesSize) As Integer
        For _idx As Integer = 0 To _recentPurchasesSize
            recentPurchases(_idx) = CInt(Console.ReadLine())
        Next _idx

        Dim _result As Integer() = _productInventory.order(recentPurchases)

        Console.WriteLine(_result.Length)
        For Each _it As Integer In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function


    Public Sub Main()
        Try
            Dim _productInventory As New ProductInventory()

            CallInit(_productInventory)
            
            For i As Integer = 1 To 100
                CallOrder(_productInventory)
            Next i

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module